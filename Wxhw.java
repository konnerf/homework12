import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.net.URL;

import java.text.DateFormat;  
import java.text.SimpleDateFormat;  
import java.util.Date;  
import java.util.Calendar;

/*
 * Konner Finney
 * Created 4/28/18
 * 
 * Documentation for prog.
 * api url api.openweathermap.org/data/2.5/weather?zip={zip code},us
 * 
 * need to display:
 * Location(city, state)
 * Time(last updated)
 * Weather  [weather]{0}[description]
 * Temperature F {main}[temp]
 * Wind from [wind][deg] degrees at [wind][speed] mph
 * Pressure inHG [main][pressure] 
 */
public class Wxhw extends Application{
	public String getURL(String strZipInp)
	{
		final String access_token = "6e6d0473634129654bb71b4807f39c73";
		System.out.println(strZipInp);
	JsonElement jse = null;
	try
	{
		// Construct WxStation API URL
		String apiURL = "http://api.openweathermap.org/data/2.5/weather?zip="
				+ java.net.URLEncoder.encode(strZipInp, "UTF-8")
				+"&APPID="
				+ java.net.URLEncoder.encode(access_token, "UTF-8");
		URL btlyURL = new URL(apiURL);

		// Open the URL
		System.out.println(apiURL);
		InputStream is = btlyURL.openStream(); // throws an IOException
		BufferedReader br = new BufferedReader(new InputStreamReader(is));

		// Read the result into a JSON Element
		jse = new JsonParser().parse(br);
  
		// Close the connection
		is.close();
		br.close();
	}
	catch (java.io.UnsupportedEncodingException uee)
	{
		uee.printStackTrace();
	}
	catch (java.net.MalformedURLException mue)
	{
		mue.printStackTrace();
	}
	catch (java.io.IOException ioe)
	{
		ioe.printStackTrace();
	}

		
		
	if (jse != null)
	{
		if(jse.getAsJsonObject().get("cod").getAsString().equals("401"))
		{
			return "Please enter a valid zip code";	
		}
		else
		{	
			
			
			// Json Parsing for output values
			String strLocation = jse.getAsJsonObject().get("name").getAsString();
			
			
			//date formatting
			SimpleDateFormat sdf = new java.text.SimpleDateFormat("MMMM dd, hh:mm a");
			sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT-7"));
			String strTime = jse.getAsJsonObject().get("dt").getAsString();
			java.util.Date time=new java.util.Date((long)Long.parseLong(strTime)*1000);
			String strFullTime = sdf.format(time) + " PST";
			
			String strWeatherDesc = jse.getAsJsonObject().get("weather").getAsJsonArray().get(0).getAsJsonObject().get("description").getAsString();
			String strTemp = jse.getAsJsonObject().get("main").getAsJsonObject().get("temp").getAsString();
			String strFTemp = String.valueOf(toFahrenheit(Double.parseDouble(strTemp)));
			String strWindSpeed = jse.getAsJsonObject().get("wind").getAsJsonObject().get("speed").getAsString();
			String strWindDeg = jse.getAsJsonObject().get("wind").getAsJsonObject().get("deg").getAsString();
			String strWind = "From " + strWindDeg + " degrees at " + strWindSpeed + " MPH";
			String strPressurehPA = jse.getAsJsonObject().get("main").getAsJsonObject().get("pressure").getAsString();
			String strPressure = String.valueOf(Double.parseDouble(strPressurehPA)/33.863886666667);
			
			
			//System.out.println(strFTemp);
			return "java -cp.:gson-2.2.4 har Wxhw " + strZipInp +"\n"
					+"Location:     " + strLocation + "\n"
					+"Time:     " + strFullTime + "\n"
					+"Weather:     " + strWeatherDesc + "\n"
					+"Temperature F:     " + strFTemp + "\n"
					+"Wind:     " + strWind + "\n"
					+"Pressure inHG:     " + strPressure + "\n";

					
		}


	}
return null;
}
  TextField urlInp;
  Label calcOtp;
  Button btnClr;


  @Override
  public void start(Stage primaryStage) {
    // Make the controls
	  
	  
	  
	urlInp = new TextField("Enter your URL");
	calcOtp = new Label("");
    btnClr=new Button("Get shorter URL");
    
    // Center text in label
    urlInp.setAlignment(Pos.CENTER);
    btnClr.setAlignment(Pos.CENTER);
    calcOtp.setAlignment(Pos.CENTER);
    
    // Apply ccs-like style to label+
    calcOtp.setStyle("-fx-border-color: #000; -fx-padding: 5px;");

    // Make container for app
    VBox root = new VBox();
    // Put container in middle of scene
    root.setAlignment(Pos.CENTER);
    // Spacing between items
    root.setSpacing(15);
    // Add to HBox
    root.getChildren().add(urlInp);
    root.getChildren().add(calcOtp);
    root.getChildren().add(btnClr);
    
    // Set widths
    setWidths();
    //attach buttons to code in separate method
    attachCode();
    // Set the scene
    Scene scene = new Scene(root, 630, 100);
    primaryStage.setTitle("Konner Finney");
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  public void setWidths(){
    // Set widths of all controls
	urlInp.setMaxWidth(150);  
	btnClr.setPrefWidth(80);
	calcOtp.setPrefWidth(400);
	calcOtp.setPrefHeight(300);
	
  }

  public void attachCode()
  {
    // Attach actions to buttons
	  btnClr.setOnAction(e -> btnClr(e));
  }

  public void btnClr(ActionEvent e)
  {
	  // launches a new class in order to POST the request to the API Endpoint
	  Wxhw b = new Wxhw();
	  // grabs the text from the input box and puts it as a parameter in the getURL function
	  calcOtp.setText(b.getURL(urlInp.getText()));
	  
	  // Error handling and json parsing
  }


  public static void main(String[] args) {
    launch(args);
     
  }
  // convert Kelvin to Fahrenheit
  public double toFahrenheit(double Kelvin)
  {
	  double Fahrenheit = 9/5*(Kelvin-273.15) +32;
	  return Fahrenheit;
  }
}
